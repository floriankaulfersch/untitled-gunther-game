pico-8 cartridge // http://www.pico-8.com
version 29
__lua__
--untitled gunther game
--by netlight

--coordinates:
-- ^y
-- |
-- |
-- |
-- o----->x
--  \
-- 		\z
			
debug=false
gravity=0.8 
menu="menu" game="game" game_over="game_over" seed="seed"
state=menu

menu_index=1
seed_menu_index=1
digit_index=1
changing_digits=false
black=0 dark_blue=1 violet=2 dark_green=3 brown=4 dark_grey=5 grey=6 white=7 red=8 orange=9 yellow=10 green=11 blue=12 lilac=13 pink=14 skin=15
max_gravity=5
min_x=0 max_x=88 min_y=48 max_y=90 --screen coordinates, so only x and y
sky_ground_separator=52
circle=0 rectangle=1 straight_line=2
rainbow_colors={red,orange,yellow,green,blue,violet}
PI=3.1415926535
random_seed=0
animation_rnds={}
animation_rnd_index=0
intensity_incr_every=12
boid_sprites={78,79}

function start_game()
	_init()
	music(0,1000)
	state=game
end

function stop_game()
	music(-1)
	state=game_over
end

function open_seed_menu()
	state=seed
end

function open_menu()
	state=menu
end

function reset_seed()
	random_seed=0
	seed_digits={0,0,0,0,0,0}
end

function change_seed()
	changing_digits=not changing_digits
	if not changing_digits then
		accept_seed()
	end
end

function accept_seed()
	random_seed=0
	for idx,digit in pairs(seed_digits) do
		random_seed+=digit*10^(6-idx)
	end
end

menu_items={{text="start", on_press=start_game},{text="seed", on_press=open_seed_menu}}
seed_menu_items={{text="seed", on_press=change_seed,y=48},{text="reset", on_press=reset_seed,y=84},{text="back", on_press=open_menu,y=92}}
seed_digits={0,0,0,0,0,0}


function _init()
	animation_rnd_index=1
	num_walls=0
	collected_boids=0
	txt_frames=0
	for i=1,500 do
		add(animation_rnds, rnd());
	end
	game_frames=0
	rainbow_frames=0
	walls_frames=0
	intensity=1
	speed_levels={4,4.5,5,5.5,6,6.5,7,8,9,10}
	particles={}
	front_particles={}
	mountains_one={}
	mountains_two={}
	mountains_three={}
	appearing_walls={}
	walls={}
	boids={}
	collection_triangles={}
	speed=4
	points=0
	if (random_seed != 0) then
		printh("setting random seed to "..random_seed)
		srand(random_seed)
		printh("first random num: "..rnd())
	end
	init_player_states()
	init_player()
	init_mountains()
end

function init_player_states()
	running_state={
		type="running",
		sprites={0,64,128,192},
		spr_counter=0,
		enter=function(s,p)
			p.sprite_index=0
			s.spr_counter=0
			add_dust()
		end,
		update=function(s,p)
			-- update sprite index and add dust particles
			s.spr_counter+=1
			if s.spr_counter>=5 then
				p.sprite_index=(p.sprite_index+1)%(#s.sprites)
				s.spr_counter=0
				if p.sprite_index==2 then
					add_dust()
				end
			end

			move_player(s,p)
			
			if(btn(❎)) then
				return jumping_state
			end
			-- test code
			if(btnp(🅾️)) then
				p.rainbowing= not p.rainbowing
			end
			return s
		end,
		leave=function(s,p)
			add_dust()
		end,
		move=function(s,p,right,down,left,up)
			local z,x,s_x,s_z=p.z,p.x,p.s_x,p.s_z
			if right then
				x+=s_x;
				s.spr_counter+=1
			elseif left then
				x-=s_x
				s.spr_counter+=.5
			end
			if up then
				z-=s_z
			elseif down then
				z+=s_z
			end
			return {x=x,y=z}
		end
	}
	jumping_state={
		type="jumping",
		sprites={6,70,134},
		enter=function(s,p)
			p.dy-=p.jump
		end,
		update=function(s,p)
			move_player(s,p)

			-- set jumping sprites
			if (abs(p.dy)<3) then
				-- pinnacle
				p.sprite_index=1
			elseif (p.dy<0) then
				-- going up
				p.sprite_index=0
			else
				-- going down
				p.sprite_index=2
			end
			if p.dy>0 and p.y>=0 then
				return running_state
			end
			return s
		end,
		leave=function(s,p)
			p.y=0
		end,
		move=function(s,p,right,down,left,up)
			local z,x,s_x,s_z=p.z,p.x,p.s_x,p.s_z
			if right then
				x+=0.5*s_x
			elseif left then
				x-=0.5*s_x
			end
			if up then
				z-=0.5*s_z
			elseif down then
				z+=0.5*s_z
			end
			return {x=x,y=z}
		end
	}
end

-- checks buttons for movement and returns new position
-- s=state,
-- p=player
function move_player(s,p)
	local pos=s:move(p,btn(➡️),btn(⬇️),btn(⬅️),btn(⬆️))
	p.x=mid(min_x,pos.x,max_x)
	p.z=mid(min_y,pos.y,max_y)
	if (btn(➡️)) then
		p.moved_right=true;
		p.moved_left=false;
	elseif (btn(⬅️)) then
		p.moved_right=false;
		p.moved_left=true;
	else
		p.moved_right=false;
		p.moved_left=false;
	end
end

function init_player()
	player={
		x=16,
		z=64,
		y=0,
		jump=13,
		dy=max_gravity,
		sprite_index=0,
		rainbowing=false,
		s_x=1.5, --speed in x direction
		s_z=2, --speed in z direction-+
		moved_left=false,
		moved_right=false,
		state=running_state,
		tail={
			{
				x=0,
				y=0,
				rad=0,
				sin_max=1
			},
			{
				x=-2,
				y=0,
				rad=0.14,
				sin_max=1
			},
			{
				x=-2,
				y=0,
				rad=0.28,
				sin_max=1
			},
			{
				x=-1,
				y=0,
				rad=0.42,
				sin_max=2
			},
			{
				x=-1,
				y=0,
				rad=0.56,
				sin_max=1
			}
		},
		act=function(self)
			if btn(❎) and btnp(🅾️) then
				debug=not debug
			end

			local newState=self.state:update(self)
			if newState.type ~= self.state.type then
				self.state:leave(self)
				self.state=newState;
				self.state:enter(self)
			end
			self.coll_rect:update()
			self.ground_coll_rect:update()
		end,
		draw=function(self)
			local z= self.z
			local y= self.y

			if not self.rainbowing then 
				palt(pink, true)
			end
			spr(self.state.sprites[self.sprite_index+1],self.x,z+y,6,4,false,false)
			if not self.rainbowing then 
				palt()
			end
			draw_tail(self)
			if (debug) then
				print(""..player.z+26)
				draw_rectangle(self.coll_rect,red)
				draw_rectangle(self.ground_coll_rect,orange)
			end
		end
	}
	player.coll_rect=player_to_collision_rect()
	player.ground_coll_rect=player_to_ground_collision_rect()
end

function draw_tail(player)
	local previous = {x=player.x+9, y=player.z+player.y+14}
	local starting_radius=3
	local sin_mult = 1.0
	local speed_mult = 1.0
	if player.moved_left then
		sin_mult=1.1
		speed_mult= 0.8
	elseif player.moved_right then
		sin_mult=0.5
		speed_mult= 1.5
	end
	local jmp_add = ((player.y<0 and abs(player.dy)>1) and sgn(player.dy)*-1 or 0)
	for i,tailpart in pairs(player.tail) do
		local sine_add=sin(tailpart.rad)*tailpart.sin_max*sin_mult
		local current={x=previous.x+tailpart.x, y=previous.y+tailpart.y+sine_add+jmp_add}
		circfill(current.x, current.y,starting_radius,grey)
		previous=current
		starting_radius-=.5
		tailpart.rad+=0.075*speed_mult
	end
	-- cover up the first circle on the horse butt
	circfill(player.x+12,player.y+player.z+15,2,white)
	
end


function init_mountains()
	local mountain_sprite_width=32
	for i=0,5 do
		local m={x=i*mountain_sprite_width,y=sky_ground_separator-(34-flr(get_animation_rand()*16)),spr_num=204,type='sprite'}
		add(mountains_three, m)
	end
	for i=0,5 do
		local m={x=i*mountain_sprite_width,y=sky_ground_separator-2-(32-flr(get_animation_rand()*10)),spr_num=200,type='sprite'}
		add(mountains_two, m)
	end
	for i=0,2 do
		local r=flr(get_animation_rand()*32)+32
		local m={x=flr(get_animation_rand()*32)+i*64,y=sky_ground_separator+r-8,r=r,c=dark_green,type='circ'}
		add(mountains_one, m)
	end
end
-->8
--lifecycle methods

function _update()
	if state==menu then
		update_menu()
	elseif state==game then
		update_game()
	elseif state==game_over then
		update_game_over()
	elseif state==seed then
		update_seed_menu()
	end
end

function update_menu()
	if btnp(⬆️) then
		menu_index=(menu_index-1+#menu_items-1)%(#menu_items)+1
	end
	if btnp(⬇️) then
		menu_index=(menu_index)%(#menu_items)+1
	end
	if btnp(❎) then
		menu_items[menu_index].on_press()
	end
end

function update_seed_menu()
	if btnp(⬆️) then
		if changing_digits then
			seed_digits[digit_index]=(seed_digits[digit_index]+1)%10
		else
			seed_menu_index=(seed_menu_index-1+#seed_menu_items-1)%(#seed_menu_items)+1
		end
	end
	if btnp(⬇️) then
		if changing_digits then
			seed_digits[digit_index]=(seed_digits[digit_index]-1)%10
		else
			seed_menu_index=(seed_menu_index)%(#seed_menu_items)+1
		end
	end
	if changing_digits and btnp(➡️) then
		digit_index=(digit_index)%#seed_digits+1
	end
	if changing_digits and btnp(⬅️) then
		digit_index=(digit_index-1+#seed_digits-1)%#seed_digits+1
	end
	if btnp(❎) then
		seed_menu_items[seed_menu_index].on_press()
	end
end

function update_game()
	game_frames=(game_frames+1)%(60-(intensity-1)*5)
	if (game_frames==0) then
		printh("add wall")
		num_walls+=1
		if intensity>2 and num_walls%10==0 then
			add_boid()
		end
		if rnd()<0.5 then
			create_low_wall(intensity>3 and rnd()<0.2)
		else
			create_high_wall(rnd()<0.5)
		end
	end
	add_gravity();
	update_particles()
	update_walls()
	update_mountains(speed / 10.0, mountains_three)
	update_mountains(speed / 5, mountains_two)
	update_mountains(speed / 2.0, mountains_one)
	player:act()
	update_boids()
	check_collisions()
	increment_points()
	update_intensity()
	update_text_frames()
end

function update_boids()
	for boid in all(boids) do
		boid:update()
		if boid.x < -8 then
			del(boids,boid)
		end
	end
end

function update_game_over()
	if(btnp(❎)) then
		state=menu
	end
end

function increment_points()
	local increment=0.1+(speed/4)*(player.x/60)
	points+=increment
end

function update_intensity()
	if (flr(num_walls / intensity_incr_every)+1 > intensity) then
		printh("intensity increase")
		if intensity < 10 then
			intensity+=1
			speed=speed_levels[intensity]
			txt_frames=60
		end
	end
end

function _draw()
	cls()
	if (state==menu) then
		draw_menu()
	elseif (state==game) then
		draw_game()
	elseif (state==game_over) then 
		draw_game_over()
	elseif (state==seed) then
		draw_seed_menu()
	end
end

function draw_menu()
	for idx,item in pairs(menu_items) do
		local str = menu_index==idx and "-"..item.text.."-" or item.text
		print_centered(str,56+(idx-1)*8, menu_index==idx and white or red)
	end
	if random_seed > 0 then
		print("seed: "..random_seed, 64,104,red)
	end
end

function draw_seed_menu()

	for idx,digit in pairs(seed_digits) do
		print("⬆️", 32+idx*8,56,seed_menu_index==1 and changing_digits and digit_index==idx and white or red)
		print(digit,34+idx*8,64)
		print("⬇️", 32+idx*8,72)
	end
	for idx,item in pairs(seed_menu_items) do
		local str = seed_menu_index==idx and "-"..item.text.."-" or item.text
		print_centered(str,item.y, seed_menu_index==idx and white or red)
	end
end

function draw_game()
	draw_sky()
	draw_mountains(mountains_three)
	draw_mountains(mountains_two)
	draw_ground()
	draw_mountains(mountains_one)
	draw_player_shadow()
	draw_boid_shadows()
	draw_game_layer_particles()
	draw_walls_and_player()
	draw_boids()
	draw_front_layer_particles()
	--if player.rainbowing then
		--add_rainbow()
	--end
	draw_ui()
end

function draw_walls_and_player()
	local previous_z=0
	local drew_player=false
	local pz=player.z+26
	for w in all(appearing_walls) do
		if w.move_delay%10<=5 then
			draw_wall(w)
		end
	end
	for w in all(walls) do
		local wz=w.z+1
		if (wz==previous_z) then

		else
			if previous_z<pz and pz<wz and not drew_player then
				drew_player=true
				player:draw()
			end
		end
		previous_z=wz
		draw_wall(w)
	end
	if not drew_player then
		player:draw()
	end
end

function draw_boids()
	for boid in all(boids) do
		boid:draw()
	end
end

function draw_game_over()
	local pt_string = flr(points)..""
	print_centered(pt_string,50,yellow)
	local prompt = "press x to retry"
	print_centered(prompt,60,red)
end

-- draws shadow dependant on player jumping height
function draw_player_shadow()
	local z= player.z
	local y= player.y
	--shadow
	local shadow_r=y<0 and calc_shadow_radius(y) or 2
	
	for i=0,6 do
		circfill(player.x+16+i*2,z+30,shadow_r,dark_blue)
	end
end

function draw_boid_shadows()
	for boid in all(boids) do
		circfill(boid.x+1,boid.z+1,1,dark_blue)
	end
end

function calc_shadow_radius(y)
	local r=abs(flr(y/8))
	r=max(2,r)
	return r
end

function draw_ui()
	if (txt_frames>0) then
		draw_faster()
	end

	-- draw points
	local digits_amount=6
	local pt_string=flr(points)..""
	local zero_padding=""
	for i=1,digits_amount-#pt_string do
		zero_padding=zero_padding.."0"
	end
	print("score: ", 72,2,8)
	print(zero_padding..pt_string,126-digits_amount*4,2,8)

	--draw boids
	spr(78,2,0)
	print(collected_boids, 12,2,red)
	
end

function add_gravity()
	local dy = player.dy;
	local y = player.y;
	dy+= gravity;
	
	if (dy > max_gravity) then
		dy=max_gravity
	end
	
	y+=dy;
	if (y > 0) then
		y=0;
	end
	player.dy=dy;
	player.y=y;
end

-->8
--environment

function draw_sky()
	rectfill(0,0,128,sky_ground_separator,blue)
end

function draw_ground()
	rectfill(0,sky_ground_separator,128,128,dark_green)
	rectfill(0,sky_ground_separator-2,128,sky_ground_separator,dark_grey)
end

function update_mountains(spd, mountains)
	for m in all(mountains) do
		m.x-=spd;
		local switch_x=m.type=='sprite' and 32 or m.r
		if (m.x<=-switch_x) then
			m.x=128+switch_x
		end
	end
end

function draw_mountains(mountains)
	for m in all(mountains) do
		if (m.type=='sprite') then
			spr(m.spr_num,m.x,m.y,4,4)
		else
			circfill(m.x,m.y,m.r,m.c)
		end
	end
end
-->8
--particles

function add_circ_particle(x,y,r,c,filled,dx,dy,ttl,rc,rce)
	local p={
		x=x, --x-pos
		y=y, --y-pos
		r=r or 1, --radius
		c=c or white, --color
		filled=filled or true,
		dx=dx or 0, --x-movement per frame
		dy=dy or 0,	--y-movement per frame
		ttl=ttl or 30, --time-to-live (-1=wont be removed)
		tl=0, -- time lived
		rc=rc or 0, --radius change per every rce frames)
		rce=rce or 0, --amount of frames between radius changes
		type=circle
	}
	add(particles,p)
end

function add_rect_particle(x,y,w,h,c,filled,dx,dy,ttl,wc,hc,wce,hce)
	local p={
		x=x, --x-pos
		y=y, --y-pos
		w=w or 1, --width
		h=h or 1, --height
		c=c or white,
		filled=filled or true,
		dx=dx or 0, --x-movement per frame
		dy=dy or 0,	--y-movement per frame
		ttl=ttl or 30, --time-to-live (-1=wont be removed)
		tl=0, -- time lived
		wc=wc or 0, --width change
		hc=hc or 0, --height change
		wce=wce or 0, --how much frames per width change
		hce=hce or 0, --how much frames per height change
		type=rectangle
	}
	add(particles,p)
end

function update_particles()
	for p in all(particles) do
		if (p.type==circle) then
			update_circ_particle(p)
		elseif (p.type==rectangle) then
		 update_rect_particle(p)
		end
	end
	--triangles
	for tri in all(collection_triangles) do
		tri:update()
		if tri.finished then
			del(collection_triangles,tri)
		end
	end
end

function update_circ_particle(p)
	p.x+=p.dx
	p.y+=p.dy
	p.tl+=1
	if (
		p.ttl ~=-1 and p.tl>=p.ttl
		or p.x<-p.r 
		or p.y < -p.r
		or p.x > 128+p.r
		or p.y > 128+p.r) then
		del(particles,p)
	end
	if (p.rce ~= 0 and p.tl%p.rce==0) then
		p.r+=p.rc
	end
end

function update_rect_particle(p)
	p.x+=p.dx
	p.y+=p.dy
	p.tl+=1
	-- width change
	if (p.wce ~= 0 and p.tl%p.wce==0) then
		p.w+=p.wc
	end
	-- height change
	if (p.hce ~= 0 and p.tl%p.hce==0) then
		p.h+=p.hc
	end
	if (
		p.ttl ~=-1 and p.tl>=p.ttl
		or p.x<-p.w 
		or p.y < -p.h
		or p.x > 128
		or p.y > 128
	) then
		del(particles,p)
	end
end

function draw_particles(table)
	for p in all(table) do
		if (p.type==circle) then
			draw_circ_particle(p)
		elseif (p.type==rectangle) then
		 draw_rect_particle(p)
		end
	end
end

function draw_game_layer_particles()
	draw_particles(particles)
end

function draw_front_layer_particles()
	draw_particles(front_particles)
	for tri in all(collection_triangles) do
		tri:draw()
	end
end

function draw_circ_particle(p)
	if (p.filled) then
		circfill(p.x,p.y,p.r,p.c)
	else
		circ(p.x,p.y,p.r,p.c)
	end
end

function draw_rect_particle(p)
	if (p.filled) then
		rectfill(p.x,p.y,p.x+p.w,p.y+p.h,p.c)
	else
		rect(p.x,p.y,p.x+p.w,p.y+p.h,p.c)
	end
end

function add_rainbow()
	rainbow_frames=(rainbow_frames+1)%16
	local x=player.x+6
	local y=player.z+player.y +12
	local add_y=0
	local sin_add_y=flr(sin(rainbow_frames/15)*4)
	for c in all(rainbow_colors) do
		add_rect_particle(x,y+add_y+sin_add_y,4,2,c,true,-3,0,45)
		add_y+=2
	end
end

function add_dust()
	local amount=flr(get_animation_rand()*3)+1
	local x_positions={10,28}
	for i=0,amount do
		for pos in all(x_positions) do
			local x = player.x + pos-flr(get_animation_rand()*4)
			local y = player.z + 30-flr(get_animation_rand()*3)
			add_circ_particle(x,y,flr(get_animation_rand()*2)+1,skin,true,-2,-.1,15,-1,5)
		end
	end
end

function add_boid_particles(boid)
	add_rect_particle(boid.x+1,boid.z-boid.y+flr(get_animation_rand()*4),1,1,white,true,-get_animation_rand(),get_animation_rand(),15)
	add_rect_particle(boid.x+1,boid.z-boid.y+flr(get_animation_rand()*4),1,1,white,true,-get_animation_rand(),get_animation_rand(),15)
end

function add_collect_effect(boid)
	local center={x=boid.x+4,y=boid.z-boid.y+4}

	cols={lilac,dark_blue,lilac}
	for i=1,3 do
		local quadrants={{-1,-1},{-1,1},{1,-1},{1,1}}
		del(quadrants,quadrants[ceil(get_animation_rand()*#quadrants)])

		local p1=create_triangle_point(quadrants[1], center)
		local p2=create_triangle_point(quadrants[2], center)
		local p3=create_triangle_point(quadrants[3], center)

		local triangle={
			p1=p1,
			p2=p2,
			p3=p3,
			col=cols[i],
			finished=false,
			update=function(self)
				self.p1:update()
				self.p2:update()
				self.p3:update()
				if self.p1.finished and self.p2.finished and self.p3.finished then
					self.finished=true
				end
			end,
			draw=function(self)
				draw_triangle(self.p1,self.p2,self.p3,self.col)
			end
		}
		add(collection_triangles,triangle)
	end
end

function draw_triangle(p1,p2,p3,col)
	line(p1.x,p1.y,p2.x,p2.y,col)
	line(p2.x,p2.y,p3.x,p3.y,col)
	line(p3.x,p3.y,p1.x,p1.y,col)
end

function create_triangle_point(quadrant,center)
	local p={
		x=center.x+8*quadrant[1]+(flr(get_animation_rand()*9)-4),
		y=center.y+8*quadrant[2]+(flr(get_animation_rand()*9)-4),
		update=update_point,
		finished=false
	}
	p.tf=cubic_in({x=p.x,y=p.y},{x=center.x,y=center.y},1)
	return p
end

function update_point(p)
	if not p.finished then
		p.tf:update()
		p.x=p.tf.current.x
		p.y=p.tf.current.y
	
		if p.tf.finished then
			p.finished=true
		end
	end
end


-->8
--collision

function wall_to_collision_rect(wall)
	if wall.type=="high" then
		return {
			x=wall.x+6,y=wall.y+4,w=6,h=30,
			update=function(self,w)
				self.x=w.x+6
			end
		}
	elseif wall.type=="low" then
		return {
			x=wall.x+6,y=wall.y+4,w=6,h=14,
			update=function(self,w)
				self.x=w.x+6
			end
		}
	end
end

function wall_to_ground_collision_rect(w)
		return {
			x=w.x+6,y=w.z-1,w=6,h=2,
			update=function(self,w)
				self.x=w.x+6
			end
		}
end

function player_to_collision_rect()
	return {
		x=player.x+10,y=player.z+player.y+6,w=24,h=26,
		update=function(self)
			self.x=player.x+10
			self.y=player.z+player.y+6
		end
	}
end

function player_to_ground_collision_rect()
	return {
		x=player.x+10,y=player.z+28,w=24,h=4,
		update=function(self)
			self.x=player.x+10
			self.y=player.z+28
		end
	}
end

function boid_to_collision_rect(boid)
	return {
		x=boid.x+1,y=boid.z-boid.y+1,w=6,h=6,
		update=function(self,b)
			self.x=b.x+1
			self.y=b.z-b.y+1
		end
	}
end

function boid_to_ground_collision_rect(boid)
	return {
		x=boid.x-2,y=boid.z-2,w=6,h=6,
		update=function(self,b)
			self.x=b.x-2
			self.y=b.z-2
		end
	}
end

function box_hit(
	rect_1,
 rect_2)
	
	local x1=rect_1.x y1=rect_1.y w1=rect_1.w h1=rect_1.h
	local x2=rect_2.x y2=rect_2.y w2=rect_2.w h2=rect_2.h
 local hit=false
 local xd=abs((x1+(w1/2))-(x2+(w2/2)))
 local xs=w1*0.5+w2*0.5
 local yd=abs((y1+(h1/2))-(y2+(h2/2)))
 local ys=h1/2+h2/2
 if xd<xs and 
    yd<ys then 
   hit=true
 end
  
 return hit
end

function check_collisions()
	for w in all(walls) do
		if w.coll_rect then
			if (box_hit(player.ground_coll_rect,w.ground_coll_rect) and 
			box_hit(player.coll_rect,w.coll_rect)) then
				stop_game()
			end
		end
	end
	for b in all(boids) do
		if b.coll_rect then
			if (box_hit(player.ground_coll_rect,b.ground_coll_rect) and 
			box_hit(player.coll_rect,b.coll_rect)) then
				del(boids,b)
				collected_boids+=1
				points+=250
				add_collect_effect(b)
			end
		end
	end
end

-->8
--obstacles

function create_high_wall(up)
	local start_x=112
	local start_y=up and sky_ground_separator-8 or sky_ground_separator+20
	for i=1,6 do
		local y=start_y+(i-1)*4
		local z=y+33
		local w={
			type="high",
			move_delay=flr(240/speed),
			x=start_x+(i-1)*2,
			y=y,
			z=z
		}
		w.coll_rect=wall_to_collision_rect(w)
		w.ground_coll_rect=wall_to_ground_collision_rect(w)
		add(appearing_walls,w)
	end
end

function create_low_wall(up)
	for i=1,17 do
		local y=up and sky_ground_separator-32+(i-1)*4 or sky_ground_separator+(i-1)*4 
		local z=up and y+52 or y+20
		local w={
			type="low",
			move_delay=flr(240/speed),
			x=104+(i-1)*2,
			y=y,
			z=z
		}
		w.coll_rect=wall_to_collision_rect(w)
		w.ground_coll_rect=wall_to_ground_collision_rect(w)
		add(appearing_walls,w)
	end
end

function update_walls()
	for w in all(appearing_walls) do
		if w.move_delay>0 then
			w.move_delay-=1
		else
			add(walls,w)
			del(appearing_walls,w)
		end
	end
	for w in all(walls) do
		w.x-=speed
		w.coll_rect:update(w)
		w.ground_coll_rect:update(w)
		if (w.x <= -16) then
			del(walls, w)
		end
	end
end

function draw_wall(w)
	local gcr=w.ground_coll_rect
	local cr=w.coll_rect

	if w.type=="high" then
		spr(13,w.x,w.y,2,2)
		spr(29,w.x,w.y+16,2,1)
		spr(29,w.x,w.y+24,2,2)
	elseif w.type=="low" then
		--draw_shadow (only relevant for flying walls)
		rectfill(gcr.x+2,gcr.y+1,gcr.x+gcr.w+2,gcr.y+gcr.h+1,dark_blue)
		rectfill(gcr.x+3,gcr.y+3,gcr.x+gcr.w+3,gcr.y+gcr.h+3,dark_blue)

		spr(13,w.x,w.y,2,3)		
	end
	if (debug) then
		print(""..w.z, w.x+6, w.y-3, red)
		
		draw_rectangle(cr,blue)
		draw_rectangle(gcr,yellow)
	end
end

-->8
--tweening
function cubic_in(from,to,seconds)
	return {
		finished=false,
		from=from,
		to=to,
		current={x=from.x,y=from.y},
		frames=flr(seconds*30),
		current_frame=0,
		update=function(self)
			local current_frame=self.current_frame
			local frames=self.frames
			local from=self.from
			local to=self.to
			self.current.x+=(current_frame/frames)^3*(to.y-from.y)
			self.current.y+=(current_frame/frames)^3*(to.x-from.x)
			self.current_frame+=1
			if self.current_frame==frames then
				self.finished=true
			end
		end
	}
end

-->8
--helpers

function get_animation_rand()
	local returned_index=animation_rnd_index;
	animation_rnd_index=(animation_rnd_index)%(#animation_rnds)+1
	return animation_rnds[animation_rnd_index]
end

function draw_rectangle(rec,col)
	rect(rec.x,rec.y,rec.x+rec.w,rec.y+rec.h,col)
end
-->8
--ui

function print_centered(text,y,color)
	print(text,64-0.5*#text*4,y,color)
end

function update_text_frames()
	if (txt_frames>0) then
		txt_frames-=1
	end
end

function draw_faster()
	print_centered("faster", 33,orange)
	print_centered("faster", 32,yellow)
end
-->8
--game objects

function add_boid()
	local z=flr(80+rnd(40))
	local boid={
		x=128,
		y=48,
		start_y=48,
		z=z,
		sine=0,
		spr_counter=0,
		particles_spawned=false,
		update=function(self)
			self.x-=0.25
			self.spr_counter=(self.spr_counter+0.1)%2
			self.y=self.start_y+sin(self.sine)*3
			self.sine=(self.sine+0.02)%1
			self.coll_rect:update(self)
			self.ground_coll_rect:update(self)
			local spr=ceil(self.spr_counter)
			if spr==2 then
				if not self.particles_spawned then
					add_boid_particles(self)
					self.particles_spawned=true
				end
			else
				if self.particles_spawned then
					self.particles_spawned=false
				end
			end
		end,
		draw=function(self)
			local cur_spr=boid_sprites[ceil(self.spr_counter)]
			
			pal(lilac,white)
			pal(dark_blue,white)
			spr(cur_spr,self.x-1,self.z-self.y)
			spr(cur_spr,self.x+1,self.z-self.y)
			spr(cur_spr,self.x,self.z-self.y-1)
			spr(cur_spr,self.x,self.z-self.y+1)
			pal()
			
			spr(cur_spr,self.x,self.z-self.y)
			if debug then
				draw_rectangle(self.coll_rect,red)
				draw_rectangle(self.ground_coll_rect,orange)
			end
		end
	}
	boid.coll_rect=boid_to_collision_rect(boid)
	boid.ground_coll_rect=boid_to_ground_collision_rect(boid)
	add(boids,boid)
end
__gfx__
000000000000000000000000000000100000e00000000000000000000000000000000000000000007000ee000000000000000000000000000000000000000000
00000000000000000000000000020110000e0000000000000000000000000000000000000000002776ee00000000000000000000000000000000000000000000
0000000000000000000000000022202070e000000000000000000000000000000000000000001216667000000000000000000000000009999999900000000000
00000000000000000000000000112217760000000000000000000000000000000000000000012167777700000000000000000000000009999999900000000000
00000000000000000000000001101116667000000000000000000000000000000000000000011767771770000000000000000000000005999999990000000000
00000000000000000000000000000067777700000000000000000000000000000000000000117767777777000000000000000000000005999999990000000000
00000000000000000000000000000767771770000000000000000000000000000000000000077776777777700000000000000000000005599999999000000000
00000000000000000000000000007767777777000000000000000000000000000000000000777776707777500000000000000000000005599999999000000000
00000000000000000000000000077776777777700000000000000000000000000000000000777777600077700000000000000000000005544444444000000000
00000000000000000000000000777776707777500000000000000000000000000000000007777777000000000000000000000000000005544444444000000000
00000000000000000000000007777777600077700000000000000000000000000000000072777777000000000000000000000000000005544444444000000000
00000000000000000000000072777777000000000000000000000000000000000000002772277777000000000000000000000000000005544444444000000000
00000000000007720000002772777777000000000000000000000000000000000000072777277777000000000000000000000000000005544444444000000000
00000000000777727777772777277777000000000000000000000000000000000007772777727770000000000000000000000000000005544444444000000000
00000000007777727777772777277777000000000000000000000000000000007777772777772270000000000000000000000000000005544444444000000000
00000000077777727777772777727770000000000000000000000000000000027777772777777720000000000000000000000000000005544444444000000000
00000000077777727777772777772270000000000000000000000000000000727777772277777777700000000000000000000000000005544444444000000000
00000000077777727777772277777720000000000000000000000000000007727777772727277777777000000000000000000000000000544444444000000000
00000000007777727777772727777777700000000000000000000000000007727777727772766077777000000000000000000000000000544444444000000000
00000000000777772222227772277777777000000000000000000000000077727772277777066000077000000000000000000000000000044444444000000000
00000000000077777777777777766077777000000000000000000000000077772227700000006600770000000000000000000000000000044444444000000000
00000000000077776600000000066000077000000000000000000000000077777770000000006557700000000000000000000000000000000000000000000000
00000000000077766000000000006600770000000000000000000000000077777600000000666557700000000000000000000000000000000000000000000000
00000000000007760000000000006557700000000000000000000000000077776000000000660000000000000000000000000000000000000000000000000000
00000000000067700000000000666557700000000000000000000000000077766000000056000000000000000000000000000000000000000000000000000000
00000000000666770000000056660000000000000000000000000000000777660000000000000000000000000000000000000000000000000000000000000000
00000000000066776500000000000000000000000000000000000000000776600000000000000000000000000000000000000000000000000000000000000000
00000000000000677500000000000000000000000000000000000000000766000000000000000000000000000000000000000000000000000000000000000000
00000000000000007000000000000000000000000000000000000000007760000000000000000000000000000000000000000000000000000000000000000000
00000000000000000550000000000000000000000000000000000000007050000000000000000000000000000000000000000000000000000000000000000000
00000000000000000550000000000000000000000000000000000000077000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000055000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000210000000e0000000000000000000000000000000000000000100000e000000000000000000000000000000000000000d000
000000000000000000000000000121000000e0000000000000000000000000000000000000020110000e00000000000000000000000000000000000d0000dd0d
00000000000000000000000000001210070e0000000000000000000000000000000000000022202070e0000000000000000000000000000000000dd00011ddd0
0000000000000000000000000000112177600000000000000000000000000000000000000011221776000000000000000000000000000000000ddd00001ddd00
00000000000000000000000000000002666700000000000000000000000000000000000001101116667000000000000000000000000000000ddddd000ddddd00
0000000000000000000000000000000677777000000000000000000000000000000000000000006777770000000000000000000000000000d0ddddd0d0dd0000
00000000000000000000000000000076777177000000000000000000000000000000000000000767771770000000000000000000000000000d00ddd00d000000
0000000000000000000000000000077677777770000000000000000000000000000000000000776777777700000000000000000000000000000000d000000000
00000000000000000000000000077777677777770000000000000000000000000000000000077776777777700000000000000000000000000000000000000000
00000000000000000000000000777777670777750000000000000000000000000000000000777776707777500000000000000000000000000000000000000000
00000000000000000000000072777777760007770000000000000000000000000000000007777777600077700000000000000000000000000000000000000000
00000000000007720000002777277777700000000000000000000000000000000000000072777777000000000000000000000000000000000000000000000000
00000000000777727777772777277777700000000000000000000000000007720000002772777777000000000000000000000000000000000000000000000000
00000000007777727777772777727777000000000000000000000000000777727777772777277777000000000000000000000000000000000000000000000000
00000000077777727777772777727777000000000000000000000000007777727777772777277777000000000000000000000000000000000000000000000000
00000000077777727777772777772270000000000000000000000000077777727777772777727770000000000000000000000000000000000000000000000000
00000000077777727777772277777726666000000000000000000000077777727777772777772270000000000000000000000000000000000000000000000000
00000000007777727777772727777776666600000000000000000000077777727777772277777720000000000000000000000000000000000000000000000000
00000000000777772222227772277777066600000000000000000000007777727777772727777777700000000000000000000000000000000000000000000000
00000000000777777777777777767777766000000000000000000000000777772222227772277777777000000000000000000000000000000000000000000000
00000000000777766600000000000777770000000000000000000000000077777777777777766077777000000000000000000000000000000000000000000000
00000000000777066600000000000007777000000000000000000000000077776600000000066000077000000000000000000000000000000000000000000000
00000000007770066000000000000006677700000000000000000000000077766000000000006600770000000000000000000000000000000000000000000000
00000000007700066000000000000005007770000000000000000000000007760000000000006557700000000000000000000000000000000000000000000000
00000000077000006600000000000000000777000000000000000000000067700000000000666557700000000000000000000000000000000000000000000000
00000000070000000660000000000000000007700000000000000000000666770000000056660000000000000000000000000000000000000000000000000000
00000000077000000066600000000000000000750000000000000000000066776500000000000000000000000000000000000000000000000000000000000000
00000000077000000000500000000000000000550000000000000000000000677500000000000000000000000000000000000000000000000000000000000000
00000000007700000000000000000000000000000000000000000000000000007000000000000000000000000000000000000000000000000000000000000000
00000000000550000000000000000000000000000000000000000000000000000550000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000550000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000201100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000102100000e000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000011210000e0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000122070e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000112776000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000001666700000000000000000000000000000000000000000000201000000000000000000000000000000000000000000000
00000000000000000000000000000006777770000000000000000000000000000000000000000002201000e00000000000000000000000000000000000000000
00000000000000000000000000000076777177000000000000000000000000000000000000000001201000e00000000000000000000000000000000000000000
0000000000000000000000000000077677777770000000000000000000000007770000000000000122100e000000000000000000000000000000000000000000
0000000000000000000000000007777767777777000000000000000000000777777700000000000122700e000000000000000000000000000000000000000000
00000000000000000000000000777777670777750000000000000000000077777777770000000001177760000000000000000000000000000000000000000000
00000000000000000000000072777777760007770000000000000000000077777777720000000000166677000000000000000000000000000000000000000000
00000000000007720000002777277777700000000000000000000000000077777777277000000077677717700000000000000000000000000000000000000000
00000000000777727777772777277777700000000000000000000000006677777772277700000777677777770000000000000000000000000000000000000000
00000000007777727777772777727777000000000000000000000006676677777772777770007777677777777000000000000000000000000000000000000000
00000000077777727777772777727777000000000000000000000066777767777772777777272777677077775000000000000000000000000000000000000000
00000000077777727777772777772270000000000000000000000066777777777772777777272777760000777000000000000000000000000000000000000000
00000000077777727777772277777720000000000000000000000060777777777077277772777277700000000000000000000000000000000000000000000000
00000000007777727777772727777770000000000000000000000060770777770007722772777727700000000000000000000000000000000000000000000000
00000000677777772222227777777766600000000000000000000660700000000000772227777772220000000000000000000000000000000000000000000000
00000066677777777777777777777666666600000000000000005507700000000000077772777777770000000000000000000000000000000000000000000000
00000666777777000000000007777000066666600000000000000057000000000000000777277777770000000000000000000000000000000000000000000000
00006607777770000000000000770000000066666000000000000055000000000000000000727777770000000000000000000000000000000000000000000000
00666077777700000000000000770000000000660000000000000000000000000000000000000777777000000000000000000000000000000000000000000000
00660077770000000000000007770000000000000000000000000000000000000000000000000006777000000000000000000000000000000000000000000000
00000777000000000000000007700000000000000000000000000000000000000000000000000000677700000000000000000000000000000000000000000000
00000770000000000000000007700000000000000000000000000000000000000000000000000000067700000000000000000000000000000000000000000000
00007700000000000000000007700000000000000000000000000000000000000000000000000000000770000000000000000000000000000000000000000000
00077700000000000000000007700000000000000000000000000000000000000000000000000000000077000000000000000000000000000000000000000000
00057000000000000000000007700000000000000000000000000000000000000000000000000000000007700000000000000000000000000000000000000000
00055000000000000000000000770000000000000000000000000000000000000000000000000000000000750000000000000000000000000000000000000000
00000000000000000000000000055000000000000000000000000000000000000000000000000000000000550000000000000000000000000000000000000000
0000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000012000000e0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000012221070e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000121022776000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000111666700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000006777770000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000076777177000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000776777777700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000077777677777770000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000777777670777750000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000007700000000072777777760007770000000000000000000000000000000000000000000000000000000000000000000000067600000000000000
00000000007777720000002777277777700000000000000000000000000000000000000000000000000000000000000000000000000000667770000000000000
00000000077777727777772777277777700000000000000000000000000000000000000000000000000000000000000000000000000066666777000000000000
00000000077777727777772777727777000000000000000000000000000000000000000000000000000000000000000000000000000666666777700000000000
00000000777777727777772777727777000000000000000000000000000000000000000000000000000000000000000000000000000666666677770000000000
00000000777777727777772777772270000000000000000000000000000000000000000000000000056600000000000000000000006666666667777700000000
00000000777777727777772277777720000000000000000000000000000000000000000000000000555666000000000000000000066666666666777770000000
00000066677777727777772727777770000000000000000000000000000000000000000000000005555566660000000000000000666666666666777777000000
00000066777777772222227777777700000000000000000000000000000000000000000000000055555556666000000000000006666666666666777777700000
00000667777777707777777777777700000000000000000000000000000000000000000000000555555555666660000000000666666666666666667777770000
00000667777777000000000000777000000000000000000000000000000000000000005566005555555555566660000000006666666666666666666777770000
00000667777000000000000000077000000000000000000000000000000000000000055566605555555555556666000000006666666666666666666777770000
00000660770000000000000000077000000000000000000000000000000000000000555556655555555555556666600000066666666666666666666777777000
00000600077000000000000000777000000000000000000000000000000000000005555555665555555555555666660000066666666666666666666777777700
00006600007700000000055077776000000000000000000000000000000000000055555555555555555555555666666000666666666666666666666677777700
00055000007700000000057770066000000000000000000000000000000000000555555555555555555555555566666066666666666666666666666677777700
00000000057000000000000000006600000000000000000000000000000000000555555555555555555555555566666666666666666666666666666677777700
00000000055000000000000000000660000000000000000000000000000000005555555555555555555555555556666666666666666666666666666677777700
00000000000000000000000000000050000000000000000000000000000000005555555555555555565555555556666666666666666666666666666677777770
00000000000000000000000000000000000000000000000000000000000000005555555555555555566555555556666666666666666666666666666667777777
00000000000000000000000000000000000000000000000000000000000000005555555555555555666655555555666666666666666666666666666666777777
00000000000000000000000000000000000000000000000000000000000000005555555555555555666665555555566666666666666666666666666666777777
__label__
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccdcccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccddccc888cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc88cc88cc88c888c888ccccccccccc888c888c888c888c888c888ccc
cccccdddcccc8c8ccccccccccccccccccccccccccccccccccccccccccccccccccccccccc8ccc8ccc8c8c8c8c8cccc8cccccccc8c8c8c8c8c8c8c8ccc8ccc8ccc
cccdddddcccc8c8ccccccccccccccccccccccccccccccccccccccccccccccccccccccccc888c8ccc8c8c88cc88cccccccccccc8c8c8c8c8c8c8c8c888ccc8ccc
ccdcdddddccc8c8ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc8c8ccc8c8c8c8c8cccc8cccccccc8c8c8c8c8c8c8c8c8ccccc8ccc
cccdccdddccc888ccccccccccccccccccccccccccccccccccccccccccccccccccccccccc88ccc88c88cc8c8c888ccccccccccc888c888c888c888c888ccc8ccc
ccccccccdccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccc676ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccc66777cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc566ccccccccccccccccccccccccccccccccccccccccc
ccccccccccccc66666777cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc555666ccccccccccccccccccccccccccccccccccccccc
cccccccccccc66666677566ccccccccccccccccccccccccccccccccccccccccccccccccccccccccc67555556666ccccccccccccccccccccccccccccccccccccc
cccccccccccc6666666555666cccccccccccccccccccccccccccccccccccccccccccccccccccccc6655555556666cccccccccccccccccccccccccccccccccccc
ccccccccccc6666666555556666cccccccccccccccccccccccccccccccccccccccccccccccccc66655555555566666cccccccccccccccccccccccccccccccccc
cccccccccc666666655555556666ccccccccccccccccccccccccccccccccccccccccccccc556666555555555556666cccccccccccccccccccccccccccccccccc
ccccccccc666666655555555566666cccccccccccccccccc676ccccccccccccccccccccc55566665555555555556666ccccccccccccccccccccccccccccccccc
cccccccc6556666555555555556666ccccccccccccccccc66777566cccccccccccccccc5555566555555555555566666cccccccccccccccccccccccccccccccc
cccccc6655566665555555555556666cccccccccccccc666767555666ccccccccccccc555555566555555555555566666ccccccccccccccccccc566ccccccccc
ccccc665555566555555555555566666cccccccccccc662776555556666cccccccccc55555555555555555555555666666cccccccccccccc676555666ccccccc
ccccc6555555566555555555555566666ccccccccccc1216667555556666cccccccc555555555555555555555555566666ccccccccccccc333333333333333cc
cccc655555555555555555555555666666ccccccccc1216777775555566666cccccc5555555555555555555555555666666ccccccc3333333333333333333333
cccc555555555555555555555555566666ccccccc551176777177555556666ccccc55555555555555555555555555566666ccc33333333333333333333333333
6cc65555555555555555555555555666666ccccc55117767777777555556666cccc5555555555555555565555555556666633333333333333333333333333333
36655555555555555555555555555566666cccc5555777767777777555566666ccc5555555555555555566555555556663333333333333333333333333333333
33355555555555555555655555555566666ccc555577777675777755555566666cc5555555555555555666655555553333333333333333333333333333333333
33333555555555555555555555555555555555555577777765557775555555555555555555555555555555555555333333333333333333333333333333333333
33333335555555555555555555555555555555555777777755555555555555555555555555555555555555555553333333333333333333333333333333333333
33333333355555555555555555555555555555557277777755555555555555555555555555555555555555555333333333333333333333333333333333333333
33333333333333333333333333333333333333277227777733333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333333333333333337277727777733333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333366677733333777277772777333333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333666777773777777277777227333333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333336666777772777777277777772333333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333336666777772777777227777777773333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333666666677772777777272727777777733333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333336666666663772777772777276637777733333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333366666666637772777227777736633337733333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333336666663337777222773333333663377333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333666333337777777333333333655773333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333333337777763333333366655773333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333333337777633333333366333333333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333333337776633333335633333333333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333333377766333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333333377663333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333333376633333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333333776333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333333735333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333337733333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333335533333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333399999999333
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333399999999333
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333359999999933
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333359999999933
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355999999993
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355999999993
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355599999999
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355599999999
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355559999999
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355559999999
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555999999
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555999999
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555599999
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555599999
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555559999
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555559999
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555555999
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555555999
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555555599
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555555599
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555555559
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555555559
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555555555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555555555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555555555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555555555
33333333333f33333333333333333f31111111111111113333333333333333333333333333333333333333333333333333333333333333333333355555555555
3333333333fff333333333333333fff1111111111111111133333333333333333333333333333333333333333333333333333333333333333333355555555555
3333333333fff3f33333333333333ffff11111111111111133333333333333333333333333333333333333333333333333333333333333333333355555555555
33333333333f3f3333333333333311ffff1111111111111113333333333333333333333333333333333333333333333333333333333333333333355555555555
33333333333333333333333333331111f11111111111111113333333333333333333333333333333333333333333333333333333333333333333355555555555
33333333333333333333333333331111111111111111111113333333333333333333333333333333333333333333333333333333333333333333335555555555
33333333333333333333333333333111111111111111111133333333333333333333333333333333333333333333333333333333333333333333335555555555
33333333333333333333333333333111111111111111111133333333333333333333333333333333333333333333333333333333333333333333333555555555
33333333333333333333333333333331111111111111113333333333333333333333333333333333333333333333333333333333333333333333333555555555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333335555555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333335555555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333555555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333555555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333335555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333335555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333555
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333355
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333335
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333335
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333

__map__
0000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__sfx__
00090000130001b0002000025000260002700027000260002400021000120000e0000c00009000090000700023000070000700007000080000b0000f0001c0000000000000000000000000000000000000000000
001000000e1000c1000e1001110000000000001510011100131000000000000000000000000000000003160000000000000000000000000000000000000000000000000000000000000000000000000000000000
01060020116351d6031e6031e603246251e6031e623026001163511603116030360324625246031e62303600116331e6001e600116001e62511600116231160000000000001e6250000011623000000000000000
01060020116351d603116131e603116331e603116130260011633116031161324600116331e6001161311600116331e6001e600116001e62311600116231160000000000001e6230000011623000000000000000
0106002026700267002670026700297002970029700297002d7002d7002d7002d7002f7002f7002f7002f7002d7002d7002d7002d7002d7002d700247002d7002870028700267002670024700247002670026700
01060020180301803018030180300c0200c0200c0200c0250c0100c0100c0100c015180301803018030180350c0200c0200c0200c0250c0100c0100c0100c0150003000030000200002000010000100001000010
01060020180301803018030180350c0200c0200c0200c025180301803018030180350c0300c0300c0300c035180201802518020180250c0100c0100c0100c0150c0300c0300c0200c02500010000100001000010
01060000300303003528000000002b0302b0322b0322b035000000000000000000002b0302b03500000000002d0302d0350000000000290302903500000000002d03500000290302903229032290350000000000
01060000305303053530512305122b5302b5322b5322b5352b5122b51200500005002b5302b5352b5122b5122d5302d5350050000500295302953500500005002d53500500295302953530530305350050000000
01060000000000000000000000002d0302d03500000000002d0302d0322d035000002b0302b0002d0302d0322d0322d03500000000002b5000000000000000002b5352d535305350000000000000000000000000
0106000026530265322653226532265322653500500005002d5302d535135001f5002953129532295322953500500005000050000500295302953200500005000050000500005000050029530295352d5002d500
010600202b5302b5322b5222b525295012950000500005002d5302d5302d5012d5002b5342b5222b5222b5322b5222b5222b5322b5122d5002d5052d5002d505245002d500285002850526500265052450024505
0106002026030260352603226035260322603500000000002d0002d005130001f00029031290322903229035000000000000000000002d0302d03500000000000000000000000000000029030290350000000000
010600202b0352b0122b0352b0122b0352b0122b0352b0122d0312d0322d0012d0002b0312b0222b0222b0352b0022b0002b0002b0002d0352b0002b0352b000240002b0002d535260002b5352b0000000000000
0106000030035300122b0352b0122d0352d01229035290122b0352b0122603526012290352901224035240122603526012210352101224035240121f0351f0121f0351f012260352601230035300122b0352b012
010600001b7501b7501b7501b75029702297021b7501b75000700007001b7501b7501b7501b7501b7501b750007002b7001b7501b7501b7501b75028702287021b75000000000000000000000000001d75000000
0106000000000000001b7501b7501b7501b7500000000000000000000000000000000000000000000000000000000000001875018750187501875000000000001b7501b7501b7501b75000000000001b7501b750
010600201b7501b75000000000002075020750207502075000000000001f7521f7521f7521f7521f7521f7550000000000000000000000000000001d7501d7501d7521d7521d7521d75523002230022300500000
0106000000000000001b75000000000001b7501b7501b7501b75000000000001b7501b7501b7501b75000000000001b7501b7501b7501b75000000000001b7501b7501b7501b75000000000001d7501d7501b700
01060000000001f7501f75000000000000000000000000000000000000000000000000000000001875018750187501875000000000001875018750187501875000000000001b7501b7501b7501b7500000000000
010600002075020750207502075000000000001f7521f7521f7521f7521f7521f7520000000000000000000000000000001d7521d7521d7521d7521d7521d7520000000000000000000000000000000000000000
010600001e75000000000001e75000000000001e75000000000001e75000000000002075220752207502075000000000002275022750000000000000000000000000000000000000000000000000000000000000
010600001e75000000000001e75000000000001e75000000000001e75000000000001e75000000000001e7501e7501e7501e75000000000002075220752207502075000000000002275222752227502275022750
01060000227502275022750000000000000000000002075220752207502075000000000001e7501e7501e7501e750000000000000000000000000000000000000000019750197501975019750000000000000000
010600001e7501e750000000000000000000001e7501e7501e7501e75000000000002075020750207502075000000000002275222752227502275000000000001e750000001e7521e7521e7521e7521e7501e750
010600001e7501e7501e7501e7501e7501e7501e7501e7501e7501e755000000000000000000001d7521d7521d7521d7521d7501d7501d7501d7501d7501d7501d7501d7501d7501d7501d7501d7550000000000
0106000000000000001e7521e7521e7521e7521e7501e7501e7501e7501e7501e7501e7501e7501e7501e7501e7501e7550000000000000000000000000000000000000000000000000000000000000000000000
010600000f5340f5300f5300f5300f5300f5300f5300f5300f5300f5300f5300f5300f5300f5300f5300f5300f5300f5300f5300f535005000050000500005001353413532135321353013530135301353013530
010600001353013530135301353013530135301353013530135301353013520135150050000500005000050000500005001453014530145301453014530145301453014530145301453014530145301453014530
010600001453014530145201451000000000000000000000000000000016530165301653016530165301653016530165301653016530165201651000000000000000000000000000000000000000000000000000
__music__
01 02444344
00 02444344
00 02444544
00 03444544
00 0248054a
00 0247054a
00 0248054a
00 0349064a
01 0208054a
00 0207054a
00 0208054a
00 0309054a
00 0208054a
00 0207054a
00 0208054a
00 0309054a
00 020a0544
00 020b0544
00 020c0544
00 030d0644
00 020a0544
00 020b0544
00 020c0544
02 030e0644
01 41421b0f
00 41421c10
02 41421d11
01 41424312
00 41424313
02 41424314
01 41424315
00 41424316
02 41424317
00 41424318
00 41424319
02 4142431a

